<?php
/**
 * certi_autologin_admin_settings function.
 * 
 * @access public
 * @param mixed $form_state
 * @return void
 */
function certi_autologin_admin_settings($form_state) {
  $form = array();
  
  $form['certi_autologin_method'] = array(
    '#type' => 'radios',
    '#title' => t("Authentication method"),
    '#options' => array(
      CERTI_AUTOLOGIN_METHOD_USERNAME => t("Authentication via username"),
      CERTI_AUTOLOGIN_METHOD_AUTHMAP => t("Authentication via authmap")
    ),
    '#default_value' => variable_get('certi_autologin_method', CERTI_AUTOLOGIN_METHOD_USERNAME),
    '#description' => t("Choose the method which the user gonna be logged."),
    
  );
  
  $form['certi_autologin_create_account'] = array(
    '#type' => 'radios',
    '#title' => t("Account Creation"),
    '#options' => array(0 => t("Disabled"), 1 => t("Enabled")),
    '#default_value' => variable_get('certi_autologin_create_account', 0),
    '#description' => t("Automatically create accounts when they do not exist.  Account names must be unique so using email addresses is a good idea!")
  );
  
  $options = user_roles();
  $form['certi_autologin_assign_role'] = array(
    '#type' => 'select',
    '#title' => t("Account Creation Role"),
    '#options' => $options,
    '#default_value' => variable_get('certi_autologin_assign_role', 1),
    '#description' => t("When a new account is created the user will be automatically assigned to this role."),
  );

  $form['certi_autologin_login_override'] = array(
    '#type' => 'radios',
    '#title' => t("Current Login Override"),
    '#options' => array(0 => t("Disabled"), 1 => t("Enabled")),
    '#default_value' => variable_get('certi_autologin_login_override', 0),
    '#description' => t("If the user is currently logged in and this option is enabled they will be logged out and logged in as the user corresponding to their certificate.  If it is disabled they will only be logged in if they are not currently logged in.")
  );

  return system_settings_form($form);
}